//
//  RecordsViewController.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/10/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit
import CoreData

enum RecordType: Printable {
    case SpendRecordType
    case IncomeRecordType

    var description : String {
        switch self {
        case .SpendRecordType: return "SpendRecord";
        case .IncomeRecordType: return "IncomeRecord";
        }
    }
}

class RecordsViewController: UIViewController, NSFetchedResultsControllerDelegate, UISearchBarDelegate {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    lazy var coreDataStack: CoreDataStack! =  {
        return (self.tabBarController as! MainTabBarController).coreDataStack
        }()

   var fetchedResultsController: NSFetchedResultsController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let fetchRequest = NSFetchRequest(entityName: "SpendRecord")

        let nameSort = NSSortDescriptor(key: "dateCreate", ascending: false)
        fetchRequest.sortDescriptors = [nameSort]

        NSFetchedResultsController.deleteCacheWithName("SpendRecord")
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: coreDataStack.context,
            sectionNameKeyPath: "sectionIdentifier",
            cacheName: "SpendRecord")
        fetchedResultsController.delegate = self

        var error: NSError?
        if !fetchedResultsController.performFetch(&error) {
            println("Error :\(error?.localizedDescription)")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func numberOfSectionsInTableView
        (tableView: UITableView) -> Int {
            return fetchedResultsController.sections!.count
    }

    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            let sectionInfo = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
            return sectionInfo.numberOfObjects
    }

    lazy var formatter: NSDateFormatter! = {
        var matter = NSDateFormatter()
        matter.calendar = NSCalendar.currentCalendar()
        let formatTemplate = NSDateFormatter.dateFormatFromTemplate("YYYY MMMM", options: 0, locale: NSLocale.currentLocale())
        matter.dateFormat = formatTemplate
        return matter
    }()

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var theSection = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
        let numericSection = theSection.name?.toInt()
        let year = numericSection! / 1000
        let month = numericSection! - (year * 1000)
        let dateComponents = NSDateComponents()
        dateComponents.year = year
        dateComponents.month = month
        let date = NSCalendar.currentCalendar().dateFromComponents(dateComponents)
        let titleString = formatter.stringFromDate(date!)

        return titleString
    }

    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
            
            let resuseIdentifier = "recordCellIdentifier"
            
            var cell =
            tableView.dequeueReusableCellWithIdentifier(
                resuseIdentifier, forIndexPath: indexPath)
                as! RecordTableViewCell
            configureCell(cell, indexPath: indexPath)
            
            return cell
    }
    
    func configureCell(cell: RecordTableViewCell, indexPath: NSIndexPath) {
        cell.accountNameLabel.text  = (fetchedResultsController.objectAtIndexPath(indexPath) as! SpendRecord).account.name
        cell.amountLabel.text = "\((fetchedResultsController.objectAtIndexPath(indexPath) as! SpendRecord).amount)"
        cell.noteLabel.text = (fetchedResultsController.objectAtIndexPath(indexPath) as! SpendRecord).note
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let record = fetchedResultsController.objectAtIndexPath(indexPath) as! SpendRecord

            let alert = UIAlertController(title: "确认删除在账户「\(record.account.name)」上的这笔支出吗", message: "删除后无法恢复", preferredStyle: .Alert)

            let saveAction = UIAlertAction(title: "确认", style: .Destructive, handler: { (alertAction:UIAlertAction!) -> Void in
                record.account.totalAmount = NSNumber(double: record.account.totalAmount.doubleValue + record.amount)

                self.coreDataStack.context.deleteObject(record)
                self.coreDataStack.saveContext()
            })
            let cancelAction = UIAlertAction(title: "取消", style: .Cancel, handler: nil)
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }

    func tableView(tableView: UITableView,
        didSelectRowAtIndexPath indexPath: NSIndexPath) {
            let record = fetchedResultsController.objectAtIndexPath(indexPath) as! SpendRecord

            var alert = UIAlertController(title: "笔记", message: "\(record.note)", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "确定", style: .Default, handler: { (action: UIAlertAction!) -> Void in
            }))

            self.presentViewController(alert, animated: true, completion: nil)
    }
    

    // MARK - NSFetchedResultsController delegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        case .Update:
            let cell = tableView.cellForRowAtIndexPath(indexPath!) as! RecordTableViewCell
            configureCell(cell, indexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        let indexSet = NSIndexSet(index: sectionIndex)
        switch type {
        case .Insert:
            tableView.insertSections(indexSet, withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteSections(indexSet, withRowAnimation: .Automatic)
        default:
            break
        }
        
    }
    
    // MARK: - SearchBar delegate
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        NSFetchedResultsController.deleteCacheWithName("SpendRecord")
        if count(searchText) > 0 {
            let predicate = NSPredicate(format: "note CONTAINS[cd] %@ or account.name BEGINSWITH[cd] %@", searchText, searchText)
            fetchedResultsController.fetchRequest.predicate = predicate
            var error: NSError?
            if !fetchedResultsController.performFetch(&error) {
                println("Error :\(error?.localizedDescription)")
            }
        } else if count(searchText) == 0 {
            fetchedResultsController.fetchRequest.predicate = nil
            var error: NSError?
            if !fetchedResultsController.performFetch(&error) {
                println("Error :\(error?.localizedDescription)")
            }
        }
        tableView.reloadData()
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "pushToAddRecordNavVC") {
            var svc = segue.destinationViewController as! UINavigationController
            if let vc = svc.visibleViewController as? AddRecordTableViewController {
                vc.coreDataStack = coreDataStack
                vc.recordType = RecordType.SpendRecordType
            }
        }
    }

    /**
    测试记录

    :param: sender button
    */
    @IBAction func testRecords(sender: UIBarButtonItem) {

        let alert = UIAlertController(title: "输入需要生成的测试数据数量", message: "", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
        }

        let filePath = NSBundle.mainBundle().pathForResource("testdata", ofType: "txt")
        let bookString = NSString(contentsOfFile: filePath!, encoding: NSUTF8StringEncoding, error: nil)
        let bookLength = bookString?.length

        let saveAction = UIAlertAction(title: "确认", style: .Destructive, handler: { (alertAction:UIAlertAction!) -> Void in

            let nameTextField = alert.textFields![0] as! UITextField
            var totalNumber: Int! = nameTextField.text.toInt()

            while totalNumber != 0 {
                totalNumber = totalNumber - 1

                let record = NSEntityDescription.insertNewObjectForEntityForName("SpendRecord", inManagedObjectContext: self.coreDataStack.context) as! SpendRecord
                let currentAmount = self.randomInRange(1...100)
                record.amount = Double(currentAmount)
                
                let randomInterval = Double(self.randomInRange(0...999999)) * 100
                let randomDate =  NSDate(timeIntervalSinceReferenceDate: Double(randomInterval))
                record.dateCreate = randomDate
                
                let padding = self.randomInRange(0...100)
                let start = self.randomInRange(0...bookLength!-101)
                let str = bookString?.substringWithRange(NSMakeRange(start, padding))
                record.note = str!
                
                let accountFetch = NSFetchRequest(entityName: "Account")
                var error: NSError?
                if let results = self.coreDataStack.context.executeFetchRequest(accountFetch, error: &error) {
                    if let account = results[0] as? Account {
                        let randomCount = self.randomInRange(0...(results.count-1))
                        let anAccount = results[randomCount] as! Account
                        record.account = anAccount
                        let number = anAccount.totalAmount.integerValue - randomCount
                        anAccount.totalAmount = NSNumber(integer: number)
                    }
                }
                
                let recordCategoryFetch = NSFetchRequest(entityName: "SpendRecordCategory")
                if let results = self.coreDataStack.context.executeFetchRequest(recordCategoryFetch, error: &error) {
                    if let cat = results[0] as? SpendRecordCategory {
                        //record.category = cat
                        let randomCount = self.randomInRange(0...(results.count-1))
                        record.category = results[randomCount] as! SpendRecordCategory
                    }
                }
                
                let calendar = NSCalendar.currentCalendar()
                let components = calendar.components((.CalendarUnitYear | .CalendarUnitMonth), fromDate: randomDate)
                record.sectionIdentifier = "\(components.year * 1000 + components.month)"
                
                self.coreDataStack.saveContext()
            }
        })
        let cancelAction = UIAlertAction(title: "取消", style: .Cancel, handler: nil)
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func randomInRange(range: Range<Int>) -> Int {
        let count = UInt32(range.endIndex - range.startIndex)
        return Int(arc4random_uniform(count)) + range.startIndex
    }
}
