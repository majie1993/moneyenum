//
//  AddRecordTableViewController.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/10/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit
import CoreData

class AddRecordTableViewController: UITableViewController, CategoryListProtocol, RecordAmountTableViewCellProtocol, AccountCategoryListTableViewCellProtocol, AddRecordDateTableViewCellProtocol, AddNoteTableViewCellProtocol {

    var recordType: RecordType!
    var coreDataStack: CoreDataStack!
    var categoryNameArray = [String]()
    var accountNameArray = [String]()

    // 要存的
    var currentCategoryName: String!
    var currentAmount: Double!
    var currentAccountName: String!
    var currentDate: NSDate!
    var currentNote: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        let fetchRequest = NSFetchRequest(entityName: "\(recordType)"+"Category")
        categoryNameArray = [String]()
        var error: NSError?
        if let results = self.coreDataStack.context.executeFetchRequest(fetchRequest, error: &error) {
            for object in results {
                let accountCategory = object as! RecordCategory
                categoryNameArray.append(accountCategory.name)
            }
        }

        let accountFetch = NSFetchRequest(entityName: "Account")
        accountNameArray = [String]()
        if let results = self.coreDataStack.context.executeFetchRequest(accountFetch, error: &error) {
            for object in results {
                let accountCategory = object as! Account
                accountNameArray.append(accountCategory.name)
            }
        }

        currentDate = NSDate()
        currentAmount = 0
        currentNote = ""

        if categoryNameArray.count > 0 {
            currentCategoryName = categoryNameArray.first
        }
        if accountNameArray.count > 0 {
            currentAccountName = accountNameArray.first
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func cancelAddRecord(sender: UIBarButtonItem) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func saveRecord(sender: UIBarButtonItem) {
        let record = NSEntityDescription.insertNewObjectForEntityForName("SpendRecord", inManagedObjectContext: coreDataStack.context) as! SpendRecord
        record.amount = currentAmount
        record.dateCreate = currentDate
        record.note = currentNote

        println("存储记录 \(currentAmount) \(currentDate) \(currentNote)")

        let accountFetch = NSFetchRequest(entityName: "Account")
        let predict = NSPredicate(format: "name == %@", currentAccountName)
        accountFetch.predicate = predict
        var error: NSError?
        if let results = self.coreDataStack.context.executeFetchRequest(accountFetch, error: &error) {
            if let account = results[0] as? Account {
                record.account = account
                account.totalAmount = NSNumber(double: account.totalAmount.doubleValue - currentAmount)
            }
        }

        let recordCategoryFetch = NSFetchRequest(entityName: "SpendRecordCategory")
        let predict2 = NSPredicate(format: "name == %@", currentCategoryName)
        recordCategoryFetch.predicate = predict2
        if let results = self.coreDataStack.context.executeFetchRequest(recordCategoryFetch, error: &error) {
            if let cat = results[0] as? SpendRecordCategory {
                record.category = cat
            }
        }

        let calendar = NSCalendar.currentCalendar()
        let components = calendar.components((.CalendarUnitYear | .CalendarUnitMonth), fromDate: currentDate)
        record.sectionIdentifier = "\(components.year * 1000 + components.month)"

        self.coreDataStack.saveContext()
        
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        var cell : UITableViewCell?
        var identifier: String?
        switch indexPath.row {
        case 0:
            identifier = "addRecordMoneyIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
            if let newCell = cell as? RecordAmountTableViewCell {
                newCell.delegate = self
            }
        case 1:
            identifier = "addRecordCategoryIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
            if let newCell = cell as? CategoryListTableViewCell {
                newCell.delegate = self
                newCell.categoryNameArray = categoryNameArray
            }
        case 2:
            identifier = "addRecordDateIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
            if let newCell = cell as? AddRecordDateTableViewCell {
                newCell.delegate = self
            }
        case 3:
            identifier = "addRecordAccountIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
            if let newCell = cell as? AccountCategoryListTableViewCell {
                newCell.delegate = self
                newCell.accountNameArray = accountNameArray
            }
        case 4:
            identifier = "addRecordNoteIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
            if let newCell = cell as? AddNoteTableViewCell {
                newCell.delegate = self
            }
        case 5:
            identifier = "addRecordDeleteIdentifier"
            cell = tableView.dequeueReusableCellWithIdentifier(identifier!, forIndexPath: indexPath) as? UITableViewCell
        default:
            identifier = "cell"
        }
        
        return cell!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 90
            // 分类
        case 1:
            return CGFloat((categoryNameArray.count + 1) * 44)
        case 2:
            return 44
        case 3:
            return 50
        case 4:
            return 160
        case 5:
            return 44
        default:
            return 44
        }
    }
    
    // MARK: Protocol
    func presentNewCategoryAlertController(alert: UIAlertController) {
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    func didSaveNewCategoryName(cell: CategoryListTableViewCell, name: String) {
        categoryNameArray.append(name)
        cell.categoryNameArray = categoryNameArray
        
        let cateogry = NSEntityDescription.insertNewObjectForEntityForName("SpendRecordCategory", inManagedObjectContext: coreDataStack.context) as! SpendRecordCategory
        cateogry.name = name
        
        coreDataStack.saveContext()
        
        cell.tableView.reloadData()
    }
    
    func didSelectName(name: String) {
        currentCategoryName = name
    }
    
    func didGetAmountText(text: String) {
        currentAmount = (text as NSString).doubleValue
    }
    
    func didGetAccountCategoryName(name: String) {
        currentAccountName = name
    }
    
    func didGetDate(date: NSDate) {
        currentDate = date
    }
    
    func didGetNote(note: String) {
        currentNote = note
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the specified item to be editable.
    return true
    }
    */
    
    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
    if editingStyle == .Delete {
    // Delete the row from the data source
    tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
    } else if editingStyle == .Insert {
    // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }    
    }
    */
    
    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
    
    }
    */
    
    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
    // Return NO if you do not want the item to be re-orderable.
    return true
    }
    */
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    }
    */
    
}
