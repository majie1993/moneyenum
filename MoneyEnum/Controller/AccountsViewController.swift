//
//  AccountsViewController.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/18/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit
import CoreData

class AccountsViewController: UIViewController, NSFetchedResultsControllerDelegate{

    @IBOutlet weak var mySwitch: UISwitch!
    @IBOutlet weak var tableView: UITableView!
    lazy var coreDataStack: CoreDataStack! =  {
        return (self.tabBarController as! MainTabBarController).coreDataStack
    }()
    var fetchedResultsController: NSFetchedResultsController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let fetchRequest = NSFetchRequest(entityName: "Account")
        
        let nameSort = NSSortDescriptor(key: "name", ascending: false)
        fetchRequest.sortDescriptors = [nameSort]

        NSFetchedResultsController.deleteCacheWithName("Account")
        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: coreDataStack.context,
            sectionNameKeyPath: "AccountCategory.name",
            cacheName: "Account")
        fetchedResultsController.delegate = self

        var error: NSError?
        if !fetchedResultsController.performFetch(&error) {
            println("Error :\(error?.localizedDescription)")
        }
    }

    override func viewWillAppear(animated: Bool) {
        if SmileAuthenticator.hasPassword() {
            mySwitch.on = true
        } else {
            mySwitch.on = false
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "pushToAddAccountVC") {
            var svc = segue.destinationViewController as! AddAccountViewController
            svc.coreDataStack = coreDataStack
        }
    }

    // MARK - TableView delegate
    func numberOfSectionsInTableView
        (tableView: UITableView) -> Int {
            return fetchedResultsController.sections!.count
    }

    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            let sectionInfo = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
            return sectionInfo.numberOfObjects
    }

    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {

            let resuseIdentifier = "accountCellReuseIdentifier"

            var cell =
            tableView.dequeueReusableCellWithIdentifier(
                resuseIdentifier, forIndexPath: indexPath)
                as! AccountTableViewCell
            configureCell(cell, indexPath: indexPath)

            return cell
    }

    func configureCell(cell: AccountTableViewCell, indexPath: NSIndexPath) {

        let account = fetchedResultsController.objectAtIndexPath(indexPath) as! Account
        cell.nameLabel.text = account.name
        cell.totalAmountLabel.text = "\(account.totalAmount)"

        var formatter1 = NSDateFormatter()
        formatter1.dateFormat = "yyyy-M-d"
        var date = formatter1.stringFromDate(account.lastAccessed)
        cell.lastAccessedDateLabel.text = "\(date)"
    }

    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
        return sectionInfo.name
    }

    func tableView(tableView: UITableView,
        didSelectRowAtIndexPath indexPath: NSIndexPath) {

            let account = fetchedResultsController.objectAtIndexPath(indexPath) as! Account

            let optionMenu = UIAlertController(title: nil, message: "\(account.name)", preferredStyle: .ActionSheet)
            let modifyAmountAction = UIAlertAction(title: "修改余额", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in

                var alert = UIAlertController(title: "余额修改", message: "原余额为\(account.totalAmount)，请输入新余额", preferredStyle: .Alert)
                alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
                }
                alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction!) -> Void in
                    let moneyTextField = alert.textFields![0] as! UITextField
                    account.totalAmount = (moneyTextField.text as NSString).doubleValue
                    self.coreDataStack.saveContext()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) -> Void in
                }))
                self.presentViewController(alert, animated: true, completion: nil)

            })
            let modifyNameAction = UIAlertAction(title: "修改名称", style: .Default, handler: {
                (alert: UIAlertAction!) -> Void in
                var alert = UIAlertController(title: "账户名修改", message: "请输入\(account.accountCategory.name)「\(account.name)」的新名称", preferredStyle: .Alert)
                alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
                }
                alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction!) -> Void in
                    let nameTextField = alert.textFields![0] as! UITextField
                    account.name = nameTextField.text
                    self.coreDataStack.saveContext()
                }))
                alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) -> Void in
                }))
                self.presentViewController(alert, animated: true, completion: nil)

            })

            let cancelAction = UIAlertAction(title: "取消", style: .Cancel, handler: {
                (alert: UIAlertAction!) -> Void in
            })

            optionMenu.addAction(modifyAmountAction)
            optionMenu.addAction(modifyNameAction)
            optionMenu.addAction(cancelAction)
            
            self.presentViewController(optionMenu, animated: true, completion: nil)
    }

    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true;
    }

    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            let account = fetchedResultsController.objectAtIndexPath(indexPath) as! Account

            // TODO: 添加有记录无法删除的逻辑
            let alert = UIAlertController(title: "确认删除账户「\(account.name)」", message: "无法删除有支出和收入记录的账户", preferredStyle: .Alert)
            let saveAction = UIAlertAction(title: "确认", style: .Destructive, handler: { (alertAction:UIAlertAction!) -> Void in
                self.coreDataStack.context.deleteObject(account)
                self.coreDataStack.saveContext()
            })
            let cancelAction = UIAlertAction(title: "取消", style: .Cancel, handler: nil)
            alert.addAction(saveAction)
            alert.addAction(cancelAction)
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }



    // MARK - NSFetchedResultsController delegate

    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {

        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        case .Update:
            let cell = tableView.cellForRowAtIndexPath(indexPath!) as! AccountTableViewCell
            configureCell(cell, indexPath: indexPath!)
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
        default:
            break
        }
    }

    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }

    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {

        let indexSet = NSIndexSet(index: sectionIndex)
        switch type {
        case .Insert:
            tableView.insertSections(indexSet, withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteSections(indexSet, withRowAnimation: .Automatic)
        default:
            break
        }
    }


    @IBAction func clickSwitch(sender: UISwitch) {

        if sender.on {
            SmileAuthenticator.sharedInstance().securityType = SecurityType.INPUT_TWICE
        } else {
            SmileAuthenticator.sharedInstance().securityType = SecurityType.INPUT_ONCE
        }
        SmileAuthenticator.sharedInstance().presentAuthViewController()
    }
}
















