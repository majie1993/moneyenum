//
//  AddAccountViewController.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/18/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit
import CoreData

class AddAccountViewController: UITableViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    var coreDataStack: CoreDataStack!

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var totalAmountTextField: UITextField!
    @IBOutlet weak var categoryPickerView: UIPickerView!

    var _account: Account?
    var account: Account {
        if _account == nil {
            _account = NSEntityDescription.insertNewObjectForEntityForName("Account", inManagedObjectContext: coreDataStack.context) as? Account
        }
        return _account!
    }
    
    var accountCategoryArray = [String]() {
        didSet {
            self.currentCategoryName = accountCategoryArray.first
        }
    }
    var currentCategoryName: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        categoryPickerView.delegate = self
        categoryPickerView.dataSource = self

        //self.generateDebugAccountCategory()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // 刷新 账户分类
        let fetchRequest = NSFetchRequest(entityName: "AccountCategory")
        var error: NSError?
        var arr = [String]()
        if let results = self.coreDataStack.context.executeFetchRequest(fetchRequest, error: &error) {
            for object in results {
                let accountCategory = object as! AccountCategory
                arr.append(accountCategory.name)
            }
        }
        accountCategoryArray = arr
        categoryPickerView.reloadAllComponents()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "pushToAddAccountCategoryVC") {
            var svc = segue.destinationViewController as! AddAccountCategoryViewController
            svc.coreDataStack = coreDataStack
        }
    }
    
    @IBAction func clickSaveButton(sender: UIBarButtonItem) {
        
        let fetchRequest = NSFetchRequest(entityName: "AccountCategory")
        let predict = NSPredicate(format: "name == %@", currentCategoryName)
        fetchRequest.predicate = predict
        var error: NSError?
        if let results = self.coreDataStack.context.executeFetchRequest(fetchRequest, error: &error) as? [AccountCategory] {
            let category = results[0]
            let account = NSEntityDescription.insertNewObjectForEntityForName("Account", inManagedObjectContext: coreDataStack.context) as! Account
            account.name = nameTextField.text
            account.totalAmount = (totalAmountTextField.text as NSString).doubleValue
            account.accountCategory = category
            
            coreDataStack.saveContext()
            self.navigationController?.popToRootViewControllerAnimated(true)
        }
    }
    
    
    
    // MARK: PickerView Delegate
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountCategoryArray.count
    }
    
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return accountCategoryArray[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentCategoryName = accountCategoryArray[row]
    }

    // MARK: Only for debug
    func generateDebugAccountCategory() {

        let fetchRequest = NSFetchRequest(entityName: "AccountCategory")
        var error: NSError? = nil
        let results =
        coreDataStack.context.countForFetchRequest(fetchRequest,
            error: &error)
        if (results == 0) {
            let names = ["日常账户", "银行账户", "投资账户"]
            for name in names {
                let category = NSEntityDescription.insertNewObjectForEntityForName("AccountCategory",
                    inManagedObjectContext: self.coreDataStack.context) as! AccountCategory
                category.name = name
                coreDataStack.saveContext()
            }
        }
    }
    
}




























