//
//  AddAccountCategoryViewController.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/7/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit
import CoreData

class AddAccountCategoryViewController: UIViewController, NSFetchedResultsControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var coreDataStack: CoreDataStack!
    var fetchedResultsController: NSFetchedResultsController!
    
    var _accountCategory: AccountCategory?
    var accountCategory: AccountCategory {
        if _accountCategory == nil {
            _accountCategory = NSEntityDescription.insertNewObjectForEntityForName("AccountCategory", inManagedObjectContext: coreDataStack.context) as? AccountCategory
        }
        return _accountCategory!
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        let fetchRequest = NSFetchRequest(entityName: "AccountCategory")
        let nameSort = NSSortDescriptor(key: "name", ascending: false)
        fetchRequest.sortDescriptors = [nameSort]

        fetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest,
            managedObjectContext: coreDataStack.context,
            sectionNameKeyPath: nil,
            cacheName: "AccountCategory")
        fetchedResultsController.delegate = self

        var error: NSError?
        if !fetchedResultsController.performFetch(&error) {
            println("Error :\(error?.localizedDescription)")
        }

        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "categoryCell")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Target action
    
    @IBAction func addCategory(sender: UIBarButtonItem) {
        var alert = UIAlertController(title: "类别", message: "新建账户类别", preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { (textField: UITextField!) -> Void in
        }
        alert.addAction(UIAlertAction(title: "Save", style: .Default, handler: { (action: UIAlertAction!) -> Void in
            let nameTextField = alert.textFields![0] as! UITextField
            let category = NSEntityDescription.insertNewObjectForEntityForName("AccountCategory", inManagedObjectContext: self.coreDataStack.context) as! AccountCategory
            category.name = nameTextField.text
            self.coreDataStack.saveContext()
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .Default, handler: { (action: UIAlertAction!) -> Void in
        }))
        presentViewController(alert, animated: true, completion: nil)
    }

    // MARK: TableView delegate

    func numberOfSectionsInTableView
        (tableView: UITableView) -> Int {
            return fetchedResultsController.sections!.count
    }
    
    func tableView(tableView: UITableView,
        numberOfRowsInSection section: Int) -> Int {
            let sectionInfo = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
            return sectionInfo.numberOfObjects
    }
    
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath)
        -> UITableViewCell {
            let resuseIdentifier = "categoryCell"
            var cell = tableView.dequeueReusableCellWithIdentifier(resuseIdentifier, forIndexPath: indexPath) as! UITableViewCell
            configureCell(cell, indexPath: indexPath)
            
            return cell
    }
    
    func configureCell(cell: UITableViewCell, indexPath: NSIndexPath) {
        let account = fetchedResultsController.objectAtIndexPath(indexPath) as! AccountCategory
        cell.textLabel!.text = account.name
        cell.selectionStyle = .None
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let sectionInfo = fetchedResultsController.sections![section] as! NSFetchedResultsSectionInfo
        return sectionInfo.name
    }
    
    
    
    // MARK - NSFetchedResultsController delegate
    
    func controllerWillChangeContent(controller: NSFetchedResultsController) {
        tableView.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?) {
        
        switch type {
        case .Insert:
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        case .Update:
            if let cell = tableView.cellForRowAtIndexPath(indexPath!) as UITableViewCell? {
                configureCell(cell, indexPath: indexPath!)
            }
        case .Move:
            tableView.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Automatic)
            tableView.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController) {
        tableView.endUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeSection sectionInfo: NSFetchedResultsSectionInfo, atIndex sectionIndex: Int, forChangeType type: NSFetchedResultsChangeType) {
        
        let indexSet = NSIndexSet(index: sectionIndex)
        switch type {
        case .Insert:
            tableView.insertSections(indexSet, withRowAnimation: .Automatic)
        case .Delete:
            tableView.deleteSections(indexSet, withRowAnimation: .Automatic)
        default:
            break
        }
    }
}
