//
//  RecordAmountTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/27/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit


class RecordAmountTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet weak var amountTextField: UITextField!
    var delegate: RecordAmountTableViewCellProtocol! = nil

    override func awakeFromNib() {
        super.awakeFromNib()
        amountTextField.delegate = self
    }

    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {

        let newString = (textField.text as NSString).stringByReplacingCharactersInRange(range, withString: string)
        self.delegate.didGetAmountText(newString)

        return true
    }

}


protocol RecordAmountTableViewCellProtocol {
    func didGetAmountText(text: String)
}