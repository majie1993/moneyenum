//
//  Record.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/17/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class Record: NSManagedObject {

    @NSManaged var amount: Double
    @NSManaged var dateCreate: NSDate
    @NSManaged var note: String
    @NSManaged var account: Account
    @NSManaged var category: RecordCategory
    @NSManaged var sectionIdentifier: String
}
