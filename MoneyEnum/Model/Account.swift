//
//  Account.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/17/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class Account: NSManagedObject {

    @NSManaged var dateCreated: NSDate
    @NSManaged var name: String
    @NSManaged var interestRate: NSNumber
    @NSManaged var totalAmount: NSNumber
    @NSManaged var records: NSSet
    @NSManaged var currency: Currency
    @NSManaged var accountCategory: AccountCategory
    @NSManaged var lastAccessed: NSDate


    override func awakeFromInsert() {
        super.awakeFromInsert()
        
        dateCreated = NSDate()
        lastAccessed = NSDate()
    }

}
