//
//   RecordCategory.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/27/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class RecordCategory: NSManagedObject {

    @NSManaged var lastAccessed: NSDate
    @NSManaged var name: String
    @NSManaged var records: NSSet

}
