//
//  Currency.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/17/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class Currency: NSManagedObject {

    @NSManaged var code: String
    @NSManaged var symbol: String
    @NSManaged var name: String
    @NSManaged var rate: NSNumber
    @NSManaged var accounts: NSSet
    @NSManaged var usedCurrency: NSManagedObject
    @NSManaged var allCurrency: NSManagedObject

}
