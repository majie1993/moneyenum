//
//  AccountCategory.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/7/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class AccountCategory: NSManagedObject {

    @NSManaged var name: String
    @NSManaged var accounts: NSSet

}
