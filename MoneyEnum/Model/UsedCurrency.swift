//
//  UsedCurrency.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 3/17/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import Foundation
import CoreData

class UsedCurrency: NSManagedObject {

    @NSManaged var currencies: Currency

}
