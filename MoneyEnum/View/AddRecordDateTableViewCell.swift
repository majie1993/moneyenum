//
//  AddRecordDateTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/10/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit

class AddRecordDateTableViewCell: UITableViewCell {

    @IBOutlet weak var recordDateLabel: UILabel!
        var delegate: AddRecordDateTableViewCellProtocol! = nil

    var dateFormatter: NSDateFormatter = {
        var formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-M-d"
        return formatter
        }()

    var oneDayComponent: NSDateComponents = {
        var dayComponent = NSDateComponents()
        dayComponent.day = 1
        return dayComponent
    }()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        recordDateLabel.text = dateFormatter.stringFromDate(NSDate())
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    @IBAction func showLastDay(sender: AnyObject) {
        let yesterday = NSCalendar.currentCalendar().dateByAddingUnit(
            .CalendarUnitDay,
            value: -1,
            toDate: dateFormatter.dateFromString(recordDateLabel.text!)!,
            options: NSCalendarOptions(0))
        recordDateLabel.text = dateFormatter.stringFromDate(yesterday!)
        self.delegate.didGetDate(yesterday!)
    }
    @IBAction func showNextDay(sender: AnyObject) {
        let tomorrow = NSCalendar.currentCalendar().dateByAddingUnit(
            .CalendarUnitDay,
            value: 1,
            toDate: dateFormatter.dateFromString(recordDateLabel.text!)!,
            options: NSCalendarOptions(0))
        recordDateLabel.text = dateFormatter.stringFromDate(tomorrow!)
        self.delegate.didGetDate(tomorrow!)
    }
}

protocol AddRecordDateTableViewCellProtocol {
    func didGetDate(date: NSDate)
}
