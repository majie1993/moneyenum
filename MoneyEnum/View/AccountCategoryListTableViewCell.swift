//
//  AccountCategoryListTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/27/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit

class AccountCategoryListTableViewCell: UITableViewCell, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var accountCategoryPickerView: UIPickerView!
    var delegate: AccountCategoryListTableViewCellProtocol! = nil
    var accountNameArray = [String]()
    var currentAccountName:String!

    override func awakeFromNib() {
        super.awakeFromNib()
        accountCategoryPickerView.delegate = self
        accountCategoryPickerView.dataSource = self
    }

    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return accountNameArray.count
    }


    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String! {
        return accountNameArray[row]
    }

    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        currentAccountName = accountNameArray[row]
        self.delegate.didGetAccountCategoryName(currentAccountName)
    }

}


protocol AccountCategoryListTableViewCellProtocol {
    func didGetAccountCategoryName(name: String)
}