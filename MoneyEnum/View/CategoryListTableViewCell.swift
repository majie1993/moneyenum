//
//  CategoryListTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/10/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit

class CategoryListTableViewCell: UITableViewCell, UITableViewDataSource, UITableViewDelegate {

    var tableView:UITableView!
    private var currentSelectedCell: UITableViewCell?
    var delegate: CategoryListProtocol! = nil

    var categoryNameArray = [String]()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        tableView = UITableView(frame: self.frame)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.addSubview(tableView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        tableView.frame = self.bounds
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCellWithIdentifier("cell", forIndexPath: indexPath) as! UITableViewCell
        cell.selectionStyle = .None

        // 添加新标签
        if indexPath.row == categoryNameArray.count {
            let button = UIButton()
            button.setTitle("添加新分类", forState: .Normal)
            button.setTitleColor(UIColor.greenColor(), forState: .Normal)
            button.addTarget(self, action: Selector("pushAddNewCategoryAlert"), forControlEvents: .TouchUpInside)
            button.sizeToFit()
            button.frame = CGRectMake((CGRectGetWidth(cell.frame)-CGRectGetWidth(button.frame))/2,
                (CGRectGetHeight(cell.frame)-CGRectGetHeight(button.frame))/2,
                CGRectGetWidth(button.frame),
                CGRectGetHeight(button.frame)
            )
            button.tag = 99;
            cell.addSubview(button)
        } else if indexPath.row < categoryNameArray.count {
            cell.textLabel?.text = categoryNameArray[indexPath.row];

            if cell.viewWithTag(100) == nil {
                let checkLabel = UILabel(frame: CGRectMake(CGRectGetMaxX(cell.frame)-30-8, 15, 15, 15))
                checkLabel.font = UIFont.fontAwesomeOfSize(15)
                checkLabel.text = String.fontAwesomeIconWithName(FontAwesome.Check)
                checkLabel.textColor = UIColor.greenColor()
                checkLabel.hidden = true
                checkLabel.tag = 100
                cell.addSubview(checkLabel)
            }
            if cell.viewWithTag(99) != nil {
                cell.viewWithTag(99)?.removeFromSuperview()
            }
        }
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryNameArray.count + 1
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = currentSelectedCell {
            cell.viewWithTag(100)?.hidden = true
        }
        
        currentSelectedCell = tableView.cellForRowAtIndexPath(indexPath)
        currentSelectedCell!.viewWithTag(100)?.hidden = false
        if (currentSelectedCell!.viewWithTag(100) != nil) {
            self.delegate.didSelectName(currentSelectedCell!.textLabel!.text!)
        }
    }
    
    func pushAddNewCategoryAlert() {
        var alert = UIAlertController(title: "新分类", message: "分类名不能重复", preferredStyle: .Alert)
        
        var cancelAction = UIAlertAction(title: "取消", style: .Cancel, handler: nil)
        var saveAction = UIAlertAction(title: "保存", style: .Default) { (alertAction: UIAlertAction!) -> Void in
            let textField = alert.textFields![0] as! UITextField
            if count(textField.text) > 0 {
                self.delegate.didSaveNewCategoryName(self, name: textField.text)
            }
        }
        alert.addTextFieldWithConfigurationHandler { (textField:UITextField!) -> Void in
        }
        alert.addAction(cancelAction)
        alert.addAction(saveAction)
        delegate.presentNewCategoryAlertController(alert)
    }
}

protocol CategoryListProtocol {
    func presentNewCategoryAlertController(alert:UIAlertController)
    func didSaveNewCategoryName(cell: CategoryListTableViewCell, name: String)
    func didSelectName(name: String)
}







