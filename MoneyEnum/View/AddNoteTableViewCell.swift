//
//  AddNoteTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/27/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit

class AddNoteTableViewCell: UITableViewCell, UITextViewDelegate {
    var delegate: AddNoteTableViewCellProtocol! = nil

    @IBOutlet weak var noteTextView: UITextView!

    override func awakeFromNib() {
        super.awakeFromNib()
        noteTextView.delegate = self
    }

    func textViewDidEndEditing(textView: UITextView) {
        self.delegate.didGetNote(noteTextView.text)
    }

    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        let newString = (textView.text as NSString).stringByReplacingCharactersInRange(range, withString: text)
        self.delegate.didGetNote(newString)

        return true
    }

}

protocol AddNoteTableViewCellProtocol {
    func didGetNote(note: String)
}
