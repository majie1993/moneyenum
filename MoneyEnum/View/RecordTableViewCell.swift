//
//  RecordTableViewCell.swift
//  MoneyEnum
//
//  Created by Jeff Ma on 5/27/15.
//  Copyright (c) 2015 Jeff Ma. All rights reserved.
//

import UIKit

class RecordTableViewCell: UITableViewCell {

    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var accountNameLabel: UILabel!
}
