import Foundation
import CoreData

class CoreDataStack {

    var context:NSManagedObjectContext
    var psc:NSPersistentStoreCoordinator
    var model:NSManagedObjectModel
    var store:NSPersistentStore?

    static let sharedInstance = CoreDataStack()

    init() {

        let bundle = NSBundle.mainBundle()
        let modelURL =
        bundle.URLForResource("MoneyEnum", withExtension:"momd")
        model = NSManagedObjectModel(contentsOfURL: modelURL!)!

        psc = NSPersistentStoreCoordinator(managedObjectModel:model)

        context = NSManagedObjectContext()
        context.persistentStoreCoordinator = psc

        let documentsURL = applicationDocumentsDirectory()
        let storeURL =
        documentsURL.URLByAppendingPathComponent("MoneyEnum")

//        let options =
//        [NSMigratePersistentStoresAutomaticallyOption: true]

        let options =
        [NSPersistentStoreUbiquitousContentNameKey: "MoneyEnum", NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]

        var error: NSError? = nil
        store = psc.addPersistentStoreWithType(NSSQLiteStoreType,
            configuration: nil,
            URL: storeURL,
            options: options,
            error:&error)

        if store == nil {
            println("Error adding persistent store: \(error)")
            abort()
        }
    }

    func saveContext() {

        var error: NSError? = nil
        if context.hasChanges && !context.save(&error) {
            println("Could not save: \(error), \(error?.userInfo)")
        }

    }

    func applicationDocumentsDirectory() -> NSURL {

        let fileManager = NSFileManager.defaultManager()

        let urls = fileManager.URLsForDirectory(.DocumentDirectory,
            inDomains: .UserDomainMask)

        return urls[0] as! NSURL
    }
}